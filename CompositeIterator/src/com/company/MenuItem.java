package com.company;


public class MenuItem extends MenuComponent {
    private String name;
    private float price;
    private boolean vegetarian;


    public MenuItem(String name, float price, boolean vegetarian) {
        this.name = name;
        this.price = price;
        this.vegetarian = vegetarian;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public void print(){
        System.out.println("Name: "+getName());
        System.out.println("Price: "+ getPrice());
        if(isVegetarian()){
            System.out.println("The menu is vegetarian");
        }


    }
}
