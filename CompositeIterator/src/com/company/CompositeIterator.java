package com.company;

import java.util.Iterator;
import java.util.Stack;

public class CompositeIterator implements Iterator {
    Stack<MenuComponent> stack = new Stack<>();

    public CompositeIterator(Stack<MenuComponent> stack){
        this.stack = stack;
    }

    @Override
    public boolean hasNext() {
        if(stack.empty())
            return false;
        else {
            Iterator<MenuComponent> iter = (Iterator<MenuComponent>) stack.peek();
            if(!iter.hasNext()){
                stack.pop();
                return hasNext();
            }else {
                return true;
            }
        }
    }

    @Override
    public MenuComponent next() {
        if(hasNext()){
            Iterator<MenuComponent> iterator = (Iterator<MenuComponent>) stack.peek();
            MenuComponent component = iterator.next();
            stack.push(component.cre)
        }
    }
}
