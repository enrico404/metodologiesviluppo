package com.company;

public class LA_pizzaFactory extends PizzaStore{
    @Override
    public Pizza createPizza(String Type) {
        Pizza pizza;
        if (Type.equals("cheese")) {
            pizza = new Pizza("cheesePizza", 10);
        } else if (Type.equals("funghi")) {
            pizza = new Pizza("funghi", 15);
        } else
            pizza = new Pizza("margherita", 10);
        return pizza;
    }

}
