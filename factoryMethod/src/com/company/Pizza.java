package com.company;

public class Pizza {
    private String name;
    private float price;

    public Pizza(String name, float price){
        this.name = name;
        this.price = price;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void bake(){
        System.out.println("Baking pizza..");
    }

    public void cut(){
        System.out.println("Cutting pizza..");
    }
}
