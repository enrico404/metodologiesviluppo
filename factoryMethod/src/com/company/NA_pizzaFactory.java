package com.company;

public class NA_pizzaFactory extends PizzaStore {
    @Override
    public Pizza createPizza(String Type) {
        Pizza pizza;
        if(Type.equals("cheese")){
            //dovrei avere una sotto classe, NA_cheesePizza, in questo modo si mostra meglio l'indipendenza
            pizza = new Pizza("cheesePizza", 10);
        }else if(Type.equals("funghi")){
            pizza = new Pizza("funghi", 15);
        }
        else
            pizza = new Pizza("margherita", 10);


        return pizza;
    }
}
