package com.company;

import java.util.HashMap;
import java.util.Observable;

public class WheaterData extends Observable {
    private double temp;
    private double humidity;
    private HashMap<String, Double> info = new HashMap<String, Double>();

    public WheaterData(){};
    public void setMeasurmentChanged(){
        setChanged();
        notifyObservers(info);
    }

    public void setMeasurment(double temp, double humidity){
        this.temp = temp;
        this.humidity = humidity;
        info.put("Temp", temp);
        info.put("Humidity", humidity);
        setMeasurmentChanged();
    }

    public double getHumidity(){
        return humidity;

    }

    public double getTemp(){
        return temp;
    }


}
