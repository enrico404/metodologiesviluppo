package com.company;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class Display1 implements Observer {
    private Observable observable;
    private double temp;
    private double humidity;

    public  Display1(Observable obs){
        this.observable = obs;
        this.observable.addObserver(this);

    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof WheaterData){
            HashMap<String, Double> info = (HashMap<String, Double>) arg;
            this.temp = ((HashMap<String, Double>) arg).get("Temp");
            this.humidity = ((HashMap<String, Double>) arg).get("Humidity");
            display();
        }
    }

    public void display(){
        System.out.println("Valori cambiati");
        System.out.println("Temp: "+ temp);
        System.out.println("Hum: "+ humidity);

    }
}
