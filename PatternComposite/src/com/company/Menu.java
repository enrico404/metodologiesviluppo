
package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;

public class Menu extends MenuComponent{
    private String name;
    private String description;
    private ArrayList<MenuComponent> menuComponents = new ArrayList<>();

    public Menu(String name, String description){
        this.name = name;
        this.description = description;

    }


    public void add(MenuComponent menuComponent){
        this.menuComponents.add(menuComponent);

    }

    public void remove(MenuComponent menuComponent){
        menuComponents.add(menuComponent);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void print(){
        System.out.println("Name: "+getName());
        System.out.println("Description: "+ getDescription());

        Iterator<MenuComponent> iter = menuComponents.iterator();
        while(iter.hasNext()){
            MenuComponent menuComponent = iter.next();
            menuComponent.print();
        }

    }

}
