package com.company;

public class Main {

    public static void main(String[] args) {

        MenuComponent dinerMenu = new Menu("diner menu", "menu da diner");
        MenuComponent cafeMenu = new Menu("cafe menu", "menu da bar");

        MenuComponent allMenus = new Menu("ALL MENUS", "tutti i menu");

        allMenus.add(dinerMenu);
        allMenus.add(cafeMenu);

        dinerMenu.add(new MenuItem("bistecca", 20, false));
        cafeMenu.add(new MenuItem("Caffè", 1, false));
        cafeMenu.add(new MenuItem("brioche", 2, true));

        Manager manager = new Manager(allMenus);
        manager.print();

    }
}
