package com.company;


import java.util.ArrayList;
import java.util.Iterator;


public class Manager {
    private Menu1 menu1;
    private Menu2 menu2;

    public Manager(Menu1 menu1, Menu2 menu2){
        this.menu1 = menu1;
        this.menu2 = menu2;
    }

    public void print(){
        Iterator menu1Items = menu1.getMenuItems().iterator();
        Iterator menu2Items = menu2.createIterator();

        System.out.println("Menu1");
        printMenu(menu1Items);
        System.out.println("Menu2");
        printMenu(menu2Items);
    }

    public void printMenu(Iterator menu){

        while (menu.hasNext()){
            MenuItem item = (MenuItem) menu.next();
            System.out.println("Name: "+item.getName());
            System.out.println("Price: "+item.getPrice());
        }
    }

}
