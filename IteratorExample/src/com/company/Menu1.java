package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Menu1 {
    private int num_items;
    private ArrayList<MenuItem> menuItems = new ArrayList<>();

    public void add_item(MenuItem item){
        menuItems.add(item);
    }

    public int getNum_items() {
        return num_items;
    }

    public void setNum_items(int num_items) {
        this.num_items = num_items;
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
}
