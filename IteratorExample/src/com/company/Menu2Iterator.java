package com.company;

import java.util.Iterator;

public class Menu2Iterator implements Iterator {

    private MenuItem[] menuItems;
    int pos = 0;

    public Menu2Iterator(MenuItem[] menuItems){
        this.menuItems = menuItems;
    }

    @Override
    public boolean hasNext() {
        if(pos >= menuItems.length || menuItems[pos]==null){
            return false;
        }
        return true;
    }

    @Override
    public MenuItem next() {
        MenuItem item = menuItems[pos];
        pos ++;
        return item;
    }
}
