package com.company;

import java.rmi.RMISecurityManager;

public class Main {

    public static void main(String[] args) {
	    Menu1 menu1 = new Menu1();
        Menu2 menu2 = new Menu2();

        MenuItem item1 = new MenuItem("riso", 10, false);
        MenuItem item2 = new MenuItem("pollo", 15, false);
        MenuItem item3 = new MenuItem("gallette", 10, false);
        MenuItem item4 = new MenuItem("pasta", 10, true);

        menu1.add_item(item1);
        menu1.add_item(item2);

        menu2.add_item(item3);
        menu2.add_item(item4);

        Manager manager = new Manager(menu1, menu2);

        manager.print();



    }
}
