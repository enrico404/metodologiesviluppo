package com.company;

import java.util.ArrayList;
import java.util.Iterator;

public class Menu2 {
    private int num_items;
    private MenuItem[] menuItems = new MenuItem[MAXITEMS];
    static final int MAXITEMS = 6;

    public Menu2(){
        num_items = 0;
    }

    public void add_item(MenuItem item){
        if (num_items <= MAXITEMS){
            menuItems[num_items] = item;
            num_items ++;
        }
    }

    public int getNum_items() {
        return num_items;
    }

    public void setNum_items(int num_items) {
        this.num_items = num_items;
    }

   public Iterator createIterator(){
        return new Menu2Iterator(menuItems);
   }
}
