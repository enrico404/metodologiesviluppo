package com.company;

public interface USAWheaterStation {

    public double getTemp();
    public String getStationName();
}
