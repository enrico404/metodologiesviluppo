package com.company;

public class WheatherStationEU {
    private double temp;
    private String stationName;

    public WheatherStationEU(int temp, String stationName){
        this.temp = temp;
        this.stationName = stationName;

    }

    public double getTemp(){
        return temp;
    }

    public String getStationName(){
        return stationName;
    }



}
