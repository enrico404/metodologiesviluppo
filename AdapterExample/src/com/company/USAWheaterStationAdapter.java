package com.company;

public class USAWheaterStationAdapter implements USAWheaterStation {
    private WheatherStationEU wheatherStation;
    private String stationName;
    public USAWheaterStationAdapter(WheatherStationEU wheatherStation, String stationName){
        this.wheatherStation = wheatherStation;
        this.stationName = stationName;
    }



    @Override
    public double getTemp() {
        //formula conversione: (0 °C × 9/5) + 32 = 32 °F
        double temp = this.wheatherStation.getTemp();
        return ((temp * 9/5) + 32);
    }

    @Override
    public String getStationName() {
        return stationName;
    }
}
