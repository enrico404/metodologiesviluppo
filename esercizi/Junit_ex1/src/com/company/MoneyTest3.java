package com.company;

import junit.framework.TestCase;

public class MoneyTest3 extends TestCase {
    private Money m1,m2,expected;

    protected void setUp(){
        int a = (int) (Math.random()*Integer.MAX_VALUE);
        int b = (int) (Math.random()*Integer.MAX_VALUE);
        m1 = new Money(a);
        m2 = new Money(b);
        expected = new Money(a+b);

    }

    public void testAdd(){
        Money result = m1.add(m2);
        assertEquals(expected, result);
        assertTrue(expected.equals(result));
    }


}
