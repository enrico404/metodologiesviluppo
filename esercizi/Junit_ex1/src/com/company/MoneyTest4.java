package com.company;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MoneyTest4 {
    private Money m1,m2,expected;

    
    @Before
    public void createMoney(){
        int a = 1;
        int b = 2;
        m1 = new Money(a);
        m2 = new Money(b);
        expected = new Money(a+b);

    }

    @Test
    public void test(){
        Money result = m1.add(m2);
        assertEquals(expected, result);

    }
}
