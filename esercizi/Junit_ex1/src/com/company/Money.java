package com.company;

public class Money {
    private int value;

    public Money(int v){
        value = v;
    }

    public Money(){
        this(0);
    }
    public Money add(Money second){
        return new Money(value+second.value);
    }

    public boolean equals(Object o){
        if(o instanceof Money){
            return ((Money) o).value == value;
        }else return false;
    }

    public String toString(){return ""+value;}
    public int getValue(){return value;}
}

