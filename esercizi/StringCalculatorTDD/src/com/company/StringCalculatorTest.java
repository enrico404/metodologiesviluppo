package com.company;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import static org.junit.Assert.assertEquals;

public class StringCalculatorTest {

    @Test
    public void empty_string_test(){
        String inputNumbers = "";
        assertEquals(0,StringCalculator.add(inputNumbers));
    }

    @Test
    public void one_number_test(){
        String inputNumbers = "1";
        assertEquals(1, StringCalculator.add(inputNumbers));
    }

    @Test
    public void two_numbers_test(){
        String inputNumbers = "1,2";
        assertEquals(3,StringCalculator.add(inputNumbers));
    }

    @Test
    public void n_numbers_test(){
        String inputNumbers = "1,2,3,4,5,6";
        assertEquals(21,StringCalculator.add(inputNumbers));
    }

    @Test
    public void new_line_test(){
        String inputNumers = "1\n2,3";
        assertEquals(6, StringCalculator.add(inputNumers));
    }

    @Test
    public void delimiter_changer_test(){
        String inputNumbers = "//;\n1;2";
        assertEquals(3, StringCalculator.add(inputNumbers));
    }
}
