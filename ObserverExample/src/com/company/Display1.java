package com.company;

import java.io.ObjectStreamException;
import java.util.Observable;
import java.util.Observer;

public class Display1 implements Observer {

    private Observable observable;
    private double temp;
    private double humidity;

    public Display1(Observable obs){
        this.observable = obs;
        observable.addObserver(this);
    }


    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof WheaterData){
            WheaterData wd = (WheaterData)o;
            this.temp = wd.getTemp();
            this.humidity = wd.getHumidity();
            display();
        }
    }


    public void display(){
        System.out.println("Valori cambiati!");
        System.out.println("Temp: "+ temp);
        System.out.println("Hum: "+ humidity);

    }
}
