package com.company;

import java.util.Observable;

public class WheaterData extends Observable {
    private double temp;
    private double humidity;

    public WheaterData(){};
    public void setMeasurmentChanged(){
        setChanged();
        notifyObservers();
    }

    public void setMeasurment(double temp, double humidity){
        this.temp = temp;
        this.humidity = humidity;
        setMeasurmentChanged();
    }

    public double getHumidity(){
        return humidity;

    }

    public double getTemp(){
        return temp;
    }


}
