package com.company;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import static org.junit.Assert.*;

public class shoppingCartTest {
    private ShoppingCart cart;
    private Product p, p2;

    @Before
    public void createCart(){
        cart = new ShoppingCart();
        p = new Product("uova", 10);
        p2 = new Product("patate", 10);
    }

    @Test
    public void zero_items_test(){

        assertEquals(cart.getItemCount(), 0);
    }

    @Test
    public void empty_cart_test(){
        cart.empty();
        assertEquals(cart.getItemCount(), 0);

    }

    @Test
    public void add_product_test(){
        cart.addItem(p);
        assertEquals(cart.getItemCount(), 1);
    }

    @Test
    public void add_balance_test(){
        double balance = cart.getBalance();
        cart.addItem(p2);
        double tot = balance + 10;
        assertEquals(cart.getBalance(), tot, Double.MIN_VALUE);
    }

    @Test
    public void remove_item_test() throws ProductNotFoundException {
        cart.addItem(p);
        int n = cart.getItemCount();
        cart.removeItem(p);
        int expected = n-1;
        assertEquals(cart.getItemCount(), expected);

    }

    @Test(expected = ProductNotFoundException.class)
    public void exception_test() throws ProductNotFoundException {
        Product p3 = new Product("pancetta", 10);
        cart.removeItem(p3);

    }

}
